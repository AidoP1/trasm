# Assembly Format

Each line may be one of:
* Instruction
* Label
* Comment
* Whitespace



# Instruction

Format:

    ins op1 op2 op3

Operands that are left off are simply 2 bytes of 0's in the assembled instruction

Example:

    ; Add 5 and 6 and store at memory address 0x00FF
    add     D.5     D.6     Ax00FF

## Operators

    {A, D, O, +, P}{., x, /, ', :}{dec, hex, name, char, label}
    op_type        num_type       value

### Operator Types

| Name  | Description                                             |
| :---: | :------------------------------------------------------ |
| **A** | An address on the address bus, 16 bits maximum          |
| **D** | A constant value, 8 bits maximum                        |
| **O** | The output register, values ignored                     |
| **+** | An address starting from the ROM address space          |
| **P** | A pointer, an address which resolves to another address |

### Operator Values
  
| Type  |      Value      | Description                                  |
| :---: | :-------------: | :------------------------------------------- |
| **.** |     Decimal     | Any decimal integer                          |
| **x** |       Hex       | Any hexadecimal integer                      |
| **/** | Memory Location | Memory location: in, out, err                |
| **'** |    Character    | Any ascii character other than whitespace    |
| **:** |      Label      | Any label. Simply pastes its line number * 8 |



# Label

Format:

    :name

A label may be used by the label *number type* to reference a location in the ROM for easier flow control. A label is simply a number that is resolved as its significant line number multiplied by 8, as each instruction is exactly 8 bytes.



# Comment

Format:

    ; This is a comment
    set Ax0 D'_ ; Comments may also appear here, causing the rest to be skipped



# List of Instructions
| Byte  |   Name    | Description                       |
| :---: | :-------: | :-------------------------------- |
| 0x30  |  **jmp**  | Unconditional jump                |
| 0x31  |  **jmz**  | Jump to op1 if op2 is zero        |
| 0x32  |  **jme**  | Jump to op1 if op2 == op3         |
| 0x33  |  **jne**  | Jump to op1 if op2 != op3         |
| 0x40  |  **set**  | Set op1 to op2                    |
| 0x41  |  **ptr**  | Set 2 bytes of op1 to op2 and op3 |
| 0xA0  |  **add**  | Add op1 and op2, store in op3     |
| 0xA1  |  **sub**  | Subtract op1 by op2, store in op3 |
| 0xA2  |  **mul**  | Multiply op1 by op2, store in op3 |
| 0xA3  |  **div**  | Divide op1 by op2, store in op3   |
| 0xA4  |  **mod**  | Modulo op1 by op2, store in op3   |
| 0xEA  | **stop**  | Stop execution, flushing output   |
| 0xEE  |  **nop**  | Do nothing                        |
| 0xEF  | **fault** | Explicitly cause a fault          |
