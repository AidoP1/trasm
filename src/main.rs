// Assembler for tremu

mod addresses {
    pub const ROM: u16 = 32768;
    pub const STDIN: u16 = 37896;
    pub const STDOUT: u16 = 37897;
    pub const STDERR: u16 = 37898;
}

use std::{ fs::File, io::{ BufRead, BufReader, Write, BufWriter }, collections::HashMap };

#[derive(Debug)]
struct Instruction(u8, InstructionFlags, Option<Register>, Option<Register>, Option<Register>);


#[derive(Debug)]
enum Register {
    Address(u16),
    Pointer(u16),
    Data(u8),
    Output()
}

impl Register {
    fn to_bytes(self) -> (u8, u8) {
        match self {
            Register::Address(address)  => ((address >> 8) as u8, address as u8),
            Register::Pointer(address)  => ((address >> 8) as u8, address as u8),
            Register::Data(data)        => (data, 0),
            Register::Output()          => (0, 0b10000000u8),
        }
    }

    fn is_address(&self) -> bool {
        match self {
            Register::Address(_) => true,
            _                    => false
        }
    }

    fn is_pointer(&self) -> bool {
        match self {
            Register::Pointer(_) => true,
            _                    => false
        }
    }
}

#[derive(Debug)]
struct InstructionFlags {
    is_address:  (bool, bool, bool),
    is_pointer:  (bool, bool, bool),
}

impl InstructionFlags {
    fn from_byte(byte: u8) -> Self {
        Self {
            is_address: (byte & 0b10000000u8 != 0, byte & 0b01000000u8 != 0, byte & 0b00100000u8 != 0),
            is_pointer: (byte & 0b00010000u8 != 0, byte & 0b00001000u8 != 0, byte & 0b00000100u8 != 0)
        }
    }

    fn addresses(op1: (bool, bool), op2: (bool, bool), op3: (bool, bool)) -> Self {
        Self {
            is_address: (op1.0, op2.0, op3.0),
            is_pointer: (op1.1, op2.1, op3.1)
        }
    }

    fn none() -> Self {
        Self {
            is_address: (false, false, false),
            is_pointer: (false, false, false)
        }
    }

    fn to_byte(self) -> u8 {
        let (op1, op2, op3) = self.is_address;
        (
            if op1 { 0b10000000u8 } else { 0 } |
            if op2 { 0b01000000u8 } else { 0 } |
            if op3 { 0b00100000u8 } else { 0 }
        ) | {
            let (op1, op2, op3) = self.is_pointer;
            (
                if op1 { 0b00010000u8 } else { 0 } |
                if op2 { 0b00001000u8 } else { 0 } |
                if op3 { 0b00000100u8 } else { 0 }
            )
        }
        
    }
}

fn main() {
    let mut args = std::env::args().skip(1);
    let input_path = args.next().expect("Missing input and output path");
    let output_path = args.next().expect("Missing output path");

    println!("Assembling {:?}", input_path);

    let mut instructions = vec![];
    let mut labels = std::collections::HashMap::new();

    let input_file = File::open(&input_path).expect("Unable to open file");
    struct Tokens(String, [Option<(char, char, String)>; 3]);
    enum Area { Instruction, Op(usize), Invalid };
    impl Area {
        // Called when there is whitespace
        fn next(self) -> Self {
            match self {
                Area::Instruction => Area::Op(0),
                Area::Op(2) => Area::Invalid,
                Area::Op(n) => Area::Op(n  + 1),
                Area::Invalid => self,
            }
        }
    }

    // Pre-process file
    let mut instruction_count = 0;
    for line in BufReader::new(input_file).lines() {
        let line = line.unwrap();
        let line = line.trim();
        
        if line.starts_with(':') {
            let label = line.split_ascii_whitespace().next().unwrap();
            if let Some(_) = labels.insert(String::from(&label[1..]), instruction_count * 8) { 
                panic!(format!("Label '{}' already exists", &label[1..]));
            }

            continue
        } else if line.starts_with(';') {
            continue
        } else if line.len() == 0 {
            continue
        } else {
            instruction_count += 1
        }
    }

    println!("Labels = {:#?}", labels);

    // Process file
    let input_file = File::open(input_path).expect("Unable to open file");

    for (index, line) in BufReader::new(input_file).lines().enumerate() {
        let line = line.unwrap();

        if line.starts_with(':') { continue }

        let (mut valid, mut tokens) = (false, Tokens(String::new(), [None, None, None]));
        let mut area = Area::Instruction;
        for token in line.split_ascii_whitespace() {
            if token.starts_with(';') { break }
            match area {
                Area::Instruction   => tokens.0 = { valid = true; String::from(token) },
                Area::Op(n)         => {
                    let mut chars = token.bytes();
                    tokens.1[n] = Some((
                        chars.next().expect(&format!("Missing operand type for operand {}", n + 1)) as char,
                        chars.next().expect(&format!("Missing number type for operand {}", n + 1)) as char,
                        chars.map(|byte| byte as char).collect()
                    ));
                },
                Area::Invalid       => panic!("Unexpected {} on line {}", token, index + 1),
            }

            area = area.next();
        }

        // Got an instruction; Interpret as instruction
        if valid {
            instructions.push({
                let op1 = if let Some(token) = &tokens.1[0] { Some(token_to_register(token, &labels)) } else { None };
                let op2 = if let Some(token) = &tokens.1[1] { Some(token_to_register(token, &labels)) } else { None };
                let op3 = if let Some(token) = &tokens.1[2] { Some(token_to_register(token, &labels)) } else { None };
                Instruction(
                    name_to_ins(tokens.0),
                    InstructionFlags::addresses(
                        (op1.as_ref().unwrap_or(&Register::Output()).is_address(), op1.as_ref().unwrap_or(&Register::Output()).is_pointer()),
                        (op2.as_ref().unwrap_or(&Register::Output()).is_address(), op2.as_ref().unwrap_or(&Register::Output()).is_pointer()),
                        (op3.as_ref().unwrap_or(&Register::Output()).is_address(), op3.as_ref().unwrap_or(&Register::Output()).is_pointer())
                    ),
                    op1,
                    op2,
                    op3
                )
            })
        }
    }

    // Init buffer for all instructions
    let mut machine_code = Vec::with_capacity(8 * instructions.len());
    for instruction in instructions {
        machine_code.push(instruction.0);
        machine_code.push(instruction.1.to_byte());
        
        let (msb, lsb) = if let Some(register) = instruction.2 { register.to_bytes() } else { (0, 0) };
        machine_code.push(msb);
        machine_code.push(lsb);
        let (msb, lsb) = if let Some(register) = instruction.3 { register.to_bytes() } else { (0, 0) };
        machine_code.push(msb);
        machine_code.push(lsb);
        let (msb, lsb) = if let Some(register) = instruction.4 { register.to_bytes() } else { (0, 0) };
        machine_code.push(msb);
        machine_code.push(lsb);
    }

    let output_file = File::create(output_path).expect("Unable to create file");
    match BufWriter::new(output_file).write_all(machine_code.as_mut_slice()) {
        Ok(_) => println!("Succesfully wrote"),
        Err(err) => panic!("Failed to write to file: {}", err)
    };
}

fn name_to_ins(name: String) -> u8 {
    let mut name = name;
    name.make_ascii_lowercase();
    match name.as_ref() {
        // Control Instructions
        "jmp"   => 0x30,
        "jmz"   => 0x31,
        "jme"   => 0x32,
        "jne"   => 0x33,

        // Memory Instructions
        "set"   => 0x40,
        "ptr"   => 0x41,

        // Arithmetic Instructions
        "add"   => 0xA0,
        "sub"   => 0xA1,
        "mul"   => 0xA2,
        "div"   => 0xA3,
        "mod"   => 0xA4,

        // Misc Instructions
        "stop"  => 0xEA,
        "nop"   => 0xEE,
        "fault" => 0xEF,

        _       => panic!(format!("Unknown instruction '{}'", name))
    }
}

fn token_to_register(token: &(char, char, String), labels: &HashMap<String, usize>) -> Register {
    fn raw_to_num(num_type: &char, num: &String, labels: &HashMap<String, usize>) -> usize {
        match num_type {
            'x' => {
                let (mut number, mut index) = (0, num.len());
                for byte in num.as_bytes() {
                    index -= 1;
                    match byte {
                        b'0'..=b'9' => number += ((byte - b'0') as usize) << index * 4,
                        b'a'..=b'f' => number += ((byte - b'a' + 10) as usize) << index * 4,
                        b'A'..=b'F' => number += ((byte - b'A' + 10) as usize) << index * 4,
                        _ => panic!(format!("Invalid number '0x{}'", num))
                    }
                };

                number
            },
            '.' => num.parse().expect(format!("Invalid number '{}'", num).as_ref()),
            ':' => labels[num],
            '/' => match num.as_ref() {
                "in"    => addresses::STDIN as usize,
                "out"   => addresses::STDOUT as usize,
                "err"   => addresses::STDERR as usize,
                _       => panic!(format!("Invalid address space '{}'", num)),
            },
            '\'' => *num.as_bytes().first().expect("Missing character") as usize,
            _ => panic!(format!("Invalid number type '{}'",  num_type))
        }
    } 

    match token.0 {
        'A' => Register::Address(raw_to_num(&token.1, &token.2, labels) as u16),
        '+' => Register::Address(addresses::ROM + raw_to_num(&token.1, &token.2, labels) as u16),
        '*' => Register::Pointer(raw_to_num(&token.1, &token.2, labels) as u16),
        'D' => Register::Data(raw_to_num(&token.1, &token.2, labels) as u8),
        'O' => Register::Output(),
        _   => panic!(format!("Invalid operator type '{}'", token.0))
    }
}
